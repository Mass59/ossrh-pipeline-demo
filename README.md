# ossrh-pipeline-demo


## Introduction

This project is a simple Maven project that demonstrates a working setup for publishing to the
[Central Repository via OSSRH](http://central.sonatype.org/) using build automation with
[Bitbucket Pipelines](https://confluence.atlassian.com/bitbucket/bitbucket-pipelines-beta-792496469.html).

The same setup can be used for deployment to a private
[Nexus Repository Manager Pro](http://www.sonatype.com/nexus-repository-sonatype) installation facilitating the
[staging suite](http://books.sonatype.com/nexus-book/reference/staging.html) and potentially policy validation
with [Nexus Lifecycle](http://www.sonatype.com/nexus-lifecycle).

## Prerequisites

For your initial setup you just need to get a [Bitbucket account](https://bitbucket.org/) as well as an
[OSSRH account](http://central.sonatype.org/pages/ossrh-guide.html).

## Maven Project Setup

The Maven project is a single module without any profile usage. Every build can be performed with

```
mvn deploy
```

This runs through the whole build life cycle and therefore performs the following tasks:

- compile main and test Java code
- run tests
- create a Java archive JAR with the compiled code and resources 
- create a JAR containing the source code
- create JavaDoc HTML files
- create a JAR containing JavaDocs
- sign all artifacts with GPG
- install them to the local repository

The `pom.xml` file contains all necessary configuration and project metadata. This setup fulfills all the
[requirements for the deployment to the Central Repository](http://central.sonatype.org/pages/requirements.html.)

The version number determines the deployment target. Any version number ending in `-SNAPSHOT` is deployed to the
OSSRH snapshot repository. The exact URL including the path from the groupId, artifactId and version is e.g.
[https://oss.sonatype.org/content/repositories/snapshots/com/simpligility/training/ossrh-pipeline-demo/0.0.2-SNAPSHOT/](https://oss.sonatype.org/content/repositories/snapshots/com/simpligility/training/ossrh-pipeline-demo/0.0.2-SNAPSHOT/).

Any other version number constitutes a release and is deployed to a OSSRH staging repository for potential
synchronization to the Central Repository.  You can
[log into OSSRH, verify the components and manually release them as desired](http://central.sonatype.org/pages/releasing-the-deployment.html).

The actual invocation used in the Pipelines script is

```
mvn -V -B -s settings.xml deploy
```

The `-V` options triggers an output of the Maven and Java versions at the beginnging of the build. The `-B` option
suppresses the download progress logging for each required artifact. The `-s settings.xml` configuration causes
the usage of the local settings with the required credentials.

## Bitbucket Pipelines Setup

The Maven project is built by Bitbucket Pipelines with every commit. The configuration for the build is contained
in the `bitbucket-pipelines.yml` file.  This simple file specifies the Docker image used by the build. The
selected image provides Apache Maven 3.3.9, Oracle JDK 8 and the needed `gpg` and `openssl` tools on an Ubuntu
operating system.
 
In order to build the Maven project successfully additional files have been added:

The `private-key.gpg.enc` file is an openssl-encrypted, GPG key file, that contains the key required for signing
all files. The unencrypted key file can exported with gpg:

````
gpg -a --export-secret-key KEYID > private-key.gpg
````

Before checking it into the bitbucket repository, it was encrypted with openssl:

```
openssl aes-256-cbc -in private-key.gpg -out private-key.gpg.enc -pass pass:<YOUR LONG PASSWORD>
```

The password value for the openssl encryption and decryption is stored in the `OPENSSL_PWD` environment variable
configured in Bitbucket and used in the `bitbucket-pipelines.yml` script.

The `settings.xml` is configured to access environment variables for the authentication to OSSRH as well the
GPG signing:

- `OSSRH_USER_TOKEN` and `OSSRH-PWD_TOKEN`: The username and password tokens for your OSSRH account as an
  alternative to your actual username and password. You can retrieve the token values by logging into
  [OSSRH](https://oss.sonatype.org). Follow the
  [user token documentation](http://books.sonatype.com/nexus-book/reference/usertoken.html) for further details.
- `GPG_KEY`: the name of the GPG key file to use for signing e.g. `F784FAB8`
- `GPG_PWD`: the password to access the GPG key

## How it Works

Any commit to the Bitbucket project triggers a build on Bitbucket Pipelines that ends up performing the following
steps:

- a new Docker container with the specified image is started
- the project source is checked out of Bitbucket
- all environment variable defined in Pipelines are injected into the executing bash shell

The pipelines configuration `bitbucket-pipelines.yml`, then proceeed with the following steps

- the encrypted private key is decrypted with `openssl` and the password from the environment variable
- the decrypted key is imported into the local `gpg` setup
- a Maven build deploying to OSSRH using the local `setting.xml` file for authentication details

## Usage

With the setup completed you can proceed with your development. Every commit causes a deployment. Every release
version commit, creates a staging repository, that can manually inspected and the contents released to the Central
Repository.

## Possible Modifications

The current setup is simplistic on purpose and solely relies on the version number and manual release to the
Central Repository.


### Automatically Release

A simple improvement is to remove the need for manual release by changing the configuration of the Nexus Staging
Maven Plugin to

```xml
<autoReleaseAfterClose>false</autoReleaseAfterClose>
```

With this configuration every initial commit with a specific release version is published to the Central
Repository. Subsequent commits with the same version fail. Your typical flow would be

- develop with snapshot version until you are happy
- commit change to release version, which triggers the release build and deployment
- commit change to next snapshot version


### Use Branch Specific Build

Bitbucket Pipelines allows you to configure specific actions for different branches. You can use this to your
advantage by only running deployments for specific builds.

For example, you can create a release by creating a versioned branch name with a prefix like `Release-1.0.0`. This
allows you to change the setup for the non-release branches to only run to the `install` phase or isolate the
deployment into activities into a profile and only activate the profile for the specific branches:

```
pipelines:
  branches:
    Release-*:
      - step:
        script:
          - mvn -V B -s settings.xml deploy
  default:
    - step:
      script:
      - mvn -V -B install
```

You can adapt these ideas to your branching and release strategy by choosing tags over branches or multiple
different approaches for features, release, integration and maintenance branches.

If you want further validation of your project setup and automation of tag creation and pom updates, you can
integrate usage of the Maven Release Plugin, the Maven Enforcer Plugin or the Maven Versions Plugin.

## Usage with Private Nexus Repository

The same setup of a Maven project can be used with a private installation of Nexus Repository. Additional
possibilities then include:

- custom staging profiles and release flows beyond one stage
- choice of component validation performed
- possibility to perform security vulnerability, license characteristics and other policy validation steps
  automatically
- notifications via email

## Help

If you need further help with this or similar setups please check out the linked resources or contact us.

- [OSSRH-related Help](http://central.sonatype.org/pages/help.html)
- [Sonatype Support](support@sonatype.com)


## Links:

- [Atlassian](https://www.atlassian.com)
- [Sonatype](http://www.sonatype.com/)
- [Bitbucket Pipelines](https://bitbucket.org/product/features/pipelines)
- [Bitbucket Pipelines Documentation](https://confluence.atlassian.com/display/BITBUCKET/Bitbucket+Pipelines+integrations)
- [Central Repository Documentation](http://central.sonatype.org/)
- [Central Repository Documentation - OSSRH Guide Overview](http://central.sonatype.org/pages/ossrh-guide.html)
- [Central Repository Documentation - Apache Maven-specific Setup](http://central.sonatype.org/pages/apache-maven.html)
- [Central Repository Documentation - PGP Guide](http://central.sonatype.org/pages/working-with-pgp-signatures.html)
- [Easy Publishing to the Central Repository - Free Video Series](http://bit.ly/EasyPublishingToCentral)
- [Nexus Repository Documentation](http://books.sonatype.com/nexus-book/)
- [Nexus Repository Documentation - Staging](http://books.sonatype.com/nexus-book/reference/staging.html)
- [Nexus Repository Documentation - User Token](http://books.sonatype.com/nexus-book/reference/usertoken.html)
- [Final Components in Central Repository](https://repo1.maven.org/maven2/com/simpligility/training/ossrh-pipeline-demo/)

 





